FROM openjdk:8-jre-slim-buster
ADD target/scala-2.12/app.jar /app/app.jar
ENTRYPOINT ["java", "-jar", "/app/app.jar"]
