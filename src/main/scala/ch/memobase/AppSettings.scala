/*
 * Import Process Delete
 * Copyright (C) 2021 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import java.util.Properties
import scala.util.Random

trait AppSettings {
  val clientId = s"${sys.env("CLIENT_ID")}-${new Random().alphanumeric.take(5).mkString}"
  val inputTopic: String = sys.env("TOPIC_IN")
  val outputTopic: String = sys.env("TOPIC_OUT")
  val pollTimeout: Int = sys.env("POLL_TIMEOUT").toInt
  val producerProps: Properties = {
    val props = new Properties()
    props.put("bootstrap.servers", sys.env("KAFKA_BOOTSTRAP_SERVERS"))
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("batch.size", "131072")
    props.put("compression.type", "gzip")
    props
  }
  val consumerProps: Properties = {
    val props = new Properties()
    props.put("bootstrap.servers", sys.env("KAFKA_BOOTSTRAP_SERVERS"))
    props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("fetch.min.bytes", "1048576")
    props.put("group.id", clientId)
    props.put("allow.auto.create.topics", "false")
    props.put("auto.offset.reset", "earliest")
    props.put("enable.auto.commit", "true")
    props.put("max.poll.records", "5000")
    props
  }
}
