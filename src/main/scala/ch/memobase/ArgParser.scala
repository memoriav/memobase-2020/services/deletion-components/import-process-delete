/*
 * Import Process Delete
 * Copyright (C) 2021 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models._
import scopt.{OParser, OParserBuilder}

import java.util.Calendar

trait ArgParser {
  self: MsgFilter =>

  private val builder: OParserBuilder[Args] = OParser.builder[Args]

  private val parser: OParser[Unit, Args] = {
    import builder._
    OParser.sequence(
      programName("import-process-delete"),
      head("", "0.1.0"),
      opt[String]('c', "record-set-filter")
        .action((v, c) => c.copy(recordSetFilters = c.recordSetFilters :+ v))
        .valueName("<id>")
        .text("record set id filter")
        .unbounded()
        .optional(),
      opt[String]('i', "institution-filter")
        .action((v, c) => c.copy(institutionFilters = c.institutionFilters :+ v))
        .valueName("<id>")
        .text("institution id filter")
        .unbounded()
        .optional(),
      opt[String]('r', "record-filter")
        .action((v, c) => c.copy(recordFilters = c.recordFilters :+ v))
        .valueName("<id>")
        .text("record id filter")
        .unbounded()
        .optional(),
      opt[String]('s', "session-filter")
        .action((v, c) => c.copy(sessionFilters = c.sessionFilters :+ v))
        .valueName("<id>")
        .text("session id filter")
        .unbounded()
        .optional(),
      opt[Calendar]('a', "created-after")
        .action((v, c) => c.copy(createdAfterFilter = Some(v)))
        .valueName("<datetime>")
        .text("retains only records processed after timestamp")
        .maxOccurs(1)
        .optional(),
      opt[Calendar]('b', "created-before")
        .action((v, c) => c.copy(createdBeforeFilter = Some(v)))
        .valueName("<datetime>")
        .text("retains only records processed before timestamp")
        .maxOccurs(1)
        .optional(),
      opt[Unit]('d', "dry-run")
        .action((_, c) => c.copy(dryRun = true))
        .text("dry-run delete"),
      arg[String]("<session-id>")
        .action((v, c) => c.copy(sessionId = v))
        .text("session id assigned to delete message")
        .required(),
      help("help").text("prints this text")
    )
  }

  def parse(args: Array[String]): Option[(String, Set[FilterFun], Boolean)] = {
    OParser.parse(parser, args, Args()) match {
      case Some(config) => Some(
        config.sessionId,
        buildFilters(config.createdAfterFilter,
          config.createdBeforeFilter,
          config.institutionFilters,
          config.recordSetFilters,
          config.recordFilters,
          config.sessionFilters),
        config.dryRun
      )
      case None => None
    }
  }
}
