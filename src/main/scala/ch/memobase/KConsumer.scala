/*
 * Import Process Delete
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import java.time.Duration

import org.apache.kafka.clients.consumer.{ConsumerRecord, KafkaConsumer}
import org.apache.logging.log4j.scala.Logging

import scala.collection.JavaConverters._

trait KConsumer { self: Logging with AppSettings =>
  lazy private val consumer = {
    val consumer = new KafkaConsumer[String, String](consumerProps)
    logger.debug(s"Subscribing to topic $inputTopic")
    consumer.subscribe(List(inputTopic).asJava)
    consumer
  }

  def poll: Iterable[ConsumerRecord[String, String]] = {
    val records = consumer.poll(Duration.ofMillis(pollTimeout)).asScala
    logger.debug(s"${records.size} new records fetched from Kafka topic $inputTopic")
    records
  }

  def closeConsumer(): Unit = {
    logger.debug("Closing Kafka consumer instance")
    consumer.close()
  }

}
