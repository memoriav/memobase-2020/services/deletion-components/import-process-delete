/*
 * Import Process Delete
 * Copyright (C) 2021 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.DeleteMessage
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.logging.log4j.scala.Logging

import java.time.LocalDateTime


abstract class KProducer {
  self: AppSettings with Logging =>

  private lazy val producer = new KafkaProducer[String, String](producerProps)

  def sendDelete(msg: DeleteMessage, dryRun: Boolean): Unit = {
    logger.debug(s"Sending delete command for ${msg.recordId}")
    val producerRecord = new ProducerRecord[String, String](outputTopic, msg.recordId, null)
    producerRecord.headers()
      .add("recordSetId", msg.recordSetId.getBytes())
      .add("institutionId", msg.institutionId.getBytes())
      .add("sessionId", msg.sessionId.getBytes())
      .add("dryRun", List((if (dryRun) 1 else 0).toByte).toArray)
    producer.send(producerRecord)
  }

  def sendTransaction(msg: DeleteMessage): Unit = {
    import ujson._
    logger.debug(s"Log transaction for ${msg.recordId}")
    val value = Obj(
      "id" -> msg.recordId,
      "action" -> "DELETE",
      "timestamp" -> LocalDateTime.now().toString()
    ).toString
    val producerRecord = new ProducerRecord[String, String](outputTopic, msg.recordId, value)
    producerRecord.headers()
      .add("recordSetId", msg.recordSetId.getBytes())
      .add("institutionId", msg.institutionId.getBytes())
      .add("sessionId", msg.sessionId.getBytes())
    producer.send(producerRecord)
  }

  def closeProducer(): Unit = {
    logger.debug("Closing Kafka producer instance")
    producer.close()
  }
}
