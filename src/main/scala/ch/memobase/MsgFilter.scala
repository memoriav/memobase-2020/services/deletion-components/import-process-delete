/*
 * Import Process Delete
 * Copyright (C) 2021 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.Report
import org.apache.logging.log4j.scala.Logging

import java.text.SimpleDateFormat
import java.util.Calendar

trait MsgFilter {
  self: Logging =>

  private val dateFormatter = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSS")

  type FilterFun = Report => Boolean

  def buildFilters(createdAfter: Option[Calendar],
                   createdBefore: Option[Calendar],
                   institutions: Seq[String],
                   recordSets: Seq[String],
                   records: Seq[String],
                   sessions: Seq[String]): Set[FilterFun] = {
    Set(buildCreatedAfterFilter(createdAfter)) ++
      Set(buildCreatedBeforeFilter(createdBefore)) ++
      Set(buildSessionIdFilters(sessions)) ++
      Set(buildRecordIdFilters(records)) ++
      Set(buildInstitutionIdFilters(institutions)) ++
      Set(buildRecordSetIdFilters(recordSets))
  }

  private def standardiseTimestamp(calendar: Calendar): String =
    f"${calendar.get(Calendar.YEAR)}%04d-" +
      f"${calendar.get(Calendar.MONTH) + 1}%02d-" +
      f"${calendar.get(Calendar.DAY_OF_MONTH)}%02dT" +
      f"${calendar.get(Calendar.HOUR_OF_DAY)}%02d:" +
      f"${calendar.get(Calendar.MINUTE)}%02d:" +
      f"${calendar.get(Calendar.SECOND)}%02d." +
      f"${calendar.get(Calendar.MILLISECOND)}%03d"

  private def buildSessionIdFilters(sessionIds: Seq[String]): FilterFun = {
    logger.info(if (sessionIds.isEmpty) {
      "no sessionIdFilters built"
    } else {
      s"sessionIdFilters built for ${sessionIds.mkString(", ")}"
    })
    val trimmedSessionIds = sessionIds.map(_.trim)
    report => sessionIds.isEmpty || trimmedSessionIds.contains(report.sessionId.trim)
  }

  private def buildRecordSetIdFilters(recordSetIds: Seq[String]): FilterFun = {
    logger.info(if (recordSetIds.isEmpty) {
      "no recordSetIdFilters built"
    } else {
      s"recordSetIdFilters built for ${recordSetIds.mkString(", ")}"
    })
    val trimmedRecordSetIds = recordSetIds.map(_.trim)
    report => recordSetIds.isEmpty || trimmedRecordSetIds.contains(report.recordSetId.trim)
  }

  private def buildInstitutionIdFilters(institutionIds: Seq[String]): FilterFun = {
    logger.info(if (institutionIds.isEmpty) {
      "no institutionIdFilters built"
    } else {
      s"institutionIdFilters built for ${institutionIds.mkString(", ")}"
    })
    val trimmedInstitutionIds = institutionIds.map(_.trim)
    report => institutionIds.isEmpty || trimmedInstitutionIds.contains(report.institutionId.trim)
  }

  private def buildRecordIdFilters(recordIds: Seq[String]): FilterFun = {
    logger.info(if (recordIds.isEmpty) {
      "no recordIdFilters built"
    } else {
      s"recordIdFilters built for ${recordIds.mkString(", ")}"
    })
    val trimmedRecordIds = recordIds.map(_.trim)
    report => recordIds.isEmpty || trimmedRecordIds.contains(report.recordId.trim)
  }

  private def buildCreatedAfterFilter(timestamp: Option[Calendar]): FilterFun = {
    val standardisedTimestamp = timestamp.collect {
      case t => standardiseTimestamp(t)
    }
    logger.info(if (timestamp.isEmpty) {
      "no createdAfterFilter built"
    } else {
      s"createdAfterFilter built for $timestamp"
    })
    report => standardisedTimestamp.isEmpty || dateFormatter.parse(standardisedTimestamp.get).before(report.timestamp)
  }

  private def buildCreatedBeforeFilter(timestamp: Option[Calendar]): FilterFun = {
    val standardisedTimestamp = timestamp.collect {
      case t => standardiseTimestamp(t)
    }
    logger.info(if (timestamp.isEmpty) {
      "no createdBeforeFilter built"
    } else {
      s"createdBeforeFilter built for $timestamp"
    })
    report => standardisedTimestamp.isEmpty || dateFormatter.parse(standardisedTimestamp.get).after(report.timestamp)
  }

}
