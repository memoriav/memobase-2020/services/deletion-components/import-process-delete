/*
 * Import Process Delete
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import java.text.SimpleDateFormat

import ch.memobase.models.Report
import org.apache.logging.log4j.scala.Logging
import org.scalatest.funsuite.AnyFunSuite

class ArgParserTest extends AnyFunSuite {
  test("should parse correctly") {
    val argParser = new {} with MsgFilter with ArgParser with Logging {}
    val parsedArgs = argParser.parse(Array("--record-set-filter", "LS-film", "asession"))
    val date = new SimpleDateFormat("YYYY-MM-dd").parse("2021-01-02");
    val report = Report("aKey", "recId", date, "LS-film", "aInstitution", "aSession")
    assert(parsedArgs.get._2.forall(_ (report)))
  }

}
