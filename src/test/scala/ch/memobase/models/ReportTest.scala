/*
 * Import Process Delete
 * Copyright (C) 2020 Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.models

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.scalatest.funsuite.AnyFunSuite

class ReportTest extends AnyFunSuite {
  test("awefwefwefwef wef") {
    val json =
      """{
        |  "id": "id",
        |  "step": "text-file-validation",
        |  "timestamp": "2020-11-26T09:14:20.324",
        |  "status": "SUCCESS",
        |  "message": "successfully validated"
        |}""".stripMargin
    val consumerRecord = new ConsumerRecord("atopic", 0, 0, "id", json)
    consumerRecord.headers().add("recordSetId", "recordSet".getBytes())
    consumerRecord.headers().add("institutionId", "institution".getBytes())
    consumerRecord.headers().add("sessionId", "session".getBytes())
    val report = Report(consumerRecord)
    report

  }
}
